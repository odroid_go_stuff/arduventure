/*
  ARDUVENTURE: http://www.team-arg.org/ardu-manual.html

  Arduboy version 1.0:  http://www.team-arg.org/ardu-downloads.html

  MADE by TEAM a.r.g. : http://www.team-arg.org/more-about.html

  2017 -2018 JO3RI GANTOIS - GAVIN ATKIN - OLIVIER HUARD - SIEGFRIED CROES

  Additional Level Design - Jace Atkin

  Game License: MIT : https://opensource.org/licenses/MIT

  Story, characters, sprites, tiles, design and art: copyrighted to TEAM a.r.g.
*/
#include <odroid_go.h>

//determine the game
#define GAME_ID 46

#include "globals.h"
#include "songs.h"
#include "menu.h"
#include "game.h"
#include "inputs.h"
#include "text.h"
#include "inventory.h"
#include "items.h"
#include "player.h"
#include "enemies.h"
#include "battles.h"

#include <Preferences.h>
Preferences preferences;


unsigned short *buffer;  // screen buffer

typedef void (*FunctionPointer) ();

const FunctionPointer PROGMEM mainGameLoop[] = {
  stateMenuIntro,
  stateMenuMain,
  stateMenuContinue,
  stateMenuNew,
  stateMenuSound,
  stateMenuCredits,
  stateGamePlaying,
  stateGameInventory,
  stateGameEquip,
  stateGameStats,
  stateGameMap,
  stateGameOver,
  showSubMenuStuff,
  showSubMenuStuff,
  showSubMenuStuff,
  showSubMenuStuff,
  walkingThroughDoor,
  stateGameBattle,
  stateGameBoss,
  stateGameIntro,
  stateGameNew,
  stateGameSaveSoundEnd,
  stateGameSaveSoundEnd,
  stateGameSaveSoundEnd,
  stateGameObjects,
  stateGameShop,
  stateGameInn,
  battleGiveRewards,
  stateMenuReboot,
};

int main() {
  arduboy.mainNoUSB();
  return 0;
}

void setup() {
  GO.begin();
  EEPROM.begin(256);
  //GO.lcd.fillScreen(RED);
  buffer=(unsigned short *)malloc(65536);
  arduboy.boot();
  arduboy.audio.begin();
  ATM.play(titleSong);
  arduboy.setFrameRate(60);                                 // set the frame rate of the game at 60 fps
  preferences.begin("Arduventure", false);

  // turn sound off completely....
  pinMode(25, OUTPUT);
  digitalWrite(25, LOW);
  
}


void loop() {
  if (!(arduboy.nextFrame())) return;
  //arduboy.fillScreen(1);
  arduboy.pollButtons();
  //arduboy.clear();
  
  drawTiles();
  updateEyes();
  checkInputs();
  
  //((FunctionPointer) pgm_read_word(&mainGameLoop[gameState]))();
  mainGameLoop[gameState]();

  if (question) drawQuestion();
  if (yesNo) drawYesNo();
  if (flashBlack) flashScreen(BLACK);     //  in battleStart
  else if (flashWhite) flashScreen(WHITE);
  //Serial.write(arduboy.getBuffer(), 128 * 64 / 8);
  arduboy.display();
}

